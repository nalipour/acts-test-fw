# ACTS Minimal Framework

Test framework for ACTS development, not intended for production usage.

## Build requirements

In addition to the build requirements of ACTS, the test framework has the following extra dependencies:

+ [ROOT](https://root.cern.ch/) (>= 6.06.04) is used for file output
